﻿using System.Reactive.Disposables;
using System.Windows;
using FileWatcher.Services;
using FileWatcher.ViewModels;
using ReactiveUI;

namespace FileWatcher.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ReactiveWindow<MainWindowViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();
            var loggerService = new LoggerService(); ;
            var fileWatcherService = new FileWatcherService(loggerService);
            var browserService = new BrowserService();
            ViewModel = new MainWindowViewModel(fileWatcherService, browserService);

            this.WhenActivated(disposable =>
            {
                this.Bind(ViewModel, vm => vm.Path, v => v.PathTextBox.Text)
                    .DisposeWith(disposable);
                this.OneWayBind(ViewModel, vm => vm.Logs, v => v.LogsListBox.ItemsSource)
                    .DisposeWith(disposable);

                this.BindCommand(ViewModel, vm => vm.BrowseCommand, v => v.BrowseButton)
                    .DisposeWith(disposable);
                this.BindCommand(ViewModel, vm => vm.StartCommand, v => v.StartButton)
                    .DisposeWith(disposable);
                this.BindCommand(ViewModel, vm => vm.StopCommand, v => v.StopButton)
                    .DisposeWith(disposable);
                this.BindCommand(ViewModel, vm => vm.ChooseFileCommand, v => v.ChooseFileButton)
                    .DisposeWith(disposable);
            });
        }
    }
}