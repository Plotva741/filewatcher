﻿namespace FileWatcher.Interfaces
{
    public interface IBrowserService
    {
        string BrowseDirectory();

        string BrowseFile();
    }
}