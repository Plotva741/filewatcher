﻿using System.Collections.Generic;

namespace FileWatcher.Interfaces
{
    public interface ILoggerService
    {
        void SetPathToFile(string fileName);

        void Log(IEnumerable<string> logs);
    }
}