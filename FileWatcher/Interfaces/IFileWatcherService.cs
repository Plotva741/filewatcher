﻿using System;
using DynamicData;

namespace FileWatcher.Interfaces
{
    public interface IFileWatcherService
    {
        IObservable<IChangeSet<string>> Connect();

        void SetPath(string path);
        void StartWritingToFile(string fileName);
        void StopWritingToFile();
        void Start();

        void Stop();
    }
}