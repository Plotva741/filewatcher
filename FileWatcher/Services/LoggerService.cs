﻿using System.Collections.Generic;
using System.IO;
using FileWatcher.Interfaces;

namespace FileWatcher.Services
{
    public class LoggerService : ILoggerService
    {
        private StreamWriter _writer;

        public void SetPathToFile(string fileName)
        {
            _writer?.Dispose();

            _writer = new StreamWriter(fileName, append: true);
        }
        public void Log(IEnumerable<string> logs)
        {
            foreach (var log in logs)
            {
                _writer.WriteLine(log);
            }
        }
    }
}