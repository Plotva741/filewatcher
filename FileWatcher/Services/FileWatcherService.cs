﻿using System;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using DynamicData;
using FileWatcher.Interfaces;

namespace FileWatcher.Services
{
    public class FileWatcherService : IFileWatcherService
    {
        private readonly ILoggerService _loggerService;
        private readonly FileSystemWatcher _watcher = new FileSystemWatcher();
        private readonly SourceList<string> _logs = new SourceList<string>();

        private IDisposable _logToFileSubscription;

        public FileWatcherService(ILoggerService loggerService)
        {
            _loggerService = loggerService;
            _watcher.NotifyFilter = NotifyFilters.LastAccess |
                                    NotifyFilters.LastWrite |
                                    NotifyFilters.FileName |
                                    NotifyFilters.DirectoryName;
            _watcher.Filter = "*.*";
            _watcher.IncludeSubdirectories = true;

            _watcher.Changed += (sender, e) => Log($"File {e.ChangeType}: \"{e.FullPath}\"");
            _watcher.Created += (sender, e) => Log($"File Created: \"{e.FullPath}\"");
            _watcher.Deleted += (sender, e) => Log($"File Deleted: \"{e.FullPath}\"");
            _watcher.Renamed += (sender, e) => Log($"File Renamed: \"{e.OldFullPath}\" to \"{e.FullPath}\"");
        }

        private void Log(string message)
        {
            var log = $"{DateTime.Now} | {message}";
            _logs.Add(log);
        }

        public IObservable<IChangeSet<string>> Connect() => _logs.Connect();

        public void SetPath(string path)
        {
            if (_watcher.Path == path)
            {
                return;
            }

            try
            {
                _watcher.Path = path;
                Log($"Path set to \"{path}\"");
            }
            catch (ArgumentException e)
            {
                Log(e.Message);
                throw;
            }
        }
        
        public void Start()
        {
            _watcher.EnableRaisingEvents = true;
            Log("Logging started");
        }

        public void Stop()
        {
            _watcher.EnableRaisingEvents = false;
            Log("Logging stopped");
        }

        public void StartWritingToFile(string fileName)
        {
            _loggerService.SetPathToFile(fileName);
            _logToFileSubscription?.Dispose();

            _logToFileSubscription = Connect()
                .Do(l => _loggerService.Log(l.Select(c => c.Item.Current)))
                .Subscribe();
        }

        public void StopWritingToFile()
        {
            _logToFileSubscription.Dispose();
        }
    }
}