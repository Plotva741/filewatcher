﻿using System.Windows.Forms;
using FileWatcher.Interfaces;

namespace FileWatcher.Services
{
    public class BrowserService : IBrowserService
    {
        public string BrowseDirectory()
        {
            using (var dialog = new FolderBrowserDialog())
            {
                var path = string.Empty;

                var result = dialog.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(dialog.SelectedPath))
                {
                    path = dialog.SelectedPath;
                }

                return path;
            }
        }

        public string BrowseFile()
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                var fileName = string.Empty;

                var result = dialog.ShowDialog();
                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(dialog.FileName))
                {
                    fileName = dialog.FileName;
                }
                
                return fileName;
            }
        }
    }
}