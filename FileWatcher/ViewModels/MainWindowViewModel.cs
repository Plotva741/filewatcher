﻿using System;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Linq;
using DynamicData;
using FileWatcher.Interfaces;
using ReactiveUI;

namespace FileWatcher.ViewModels
{
    public class MainWindowViewModel : ReactiveObject
    {
        private readonly IFileWatcherService _fileWatcherService;
        private readonly IBrowserService _browserService;
        private readonly ReadOnlyObservableCollection<string> _logs;
        private string _path;
        private bool _started;

        public string Path
        {
            get => _path;
            set => this.RaiseAndSetIfChanged(ref _path, value);
        }
        public bool Started
        {
            get => _started;
            set => this.RaiseAndSetIfChanged(ref _started, value);
        }
        public ReadOnlyObservableCollection<string> Logs => _logs;

        public ReactiveCommand<Unit, Unit> BrowseCommand { get; }
        public ReactiveCommand<Unit, Unit> StartCommand { get; }
        public ReactiveCommand<Unit, Unit> StopCommand { get; }
        public ReactiveCommand<Unit, Unit> ChooseFileCommand { get; }

        public MainWindowViewModel(IFileWatcherService fileWatcherService, IBrowserService browserService)
        {
            _fileWatcherService = fileWatcherService;
            _browserService = browserService;
            _fileWatcherService.Connect()
                .ObserveOn(RxApp.MainThreadScheduler)
                .Bind(out _logs)
                .Subscribe();

            BrowseCommand = ReactiveCommand.Create(() => { Path = browserService.BrowseDirectory(); });
            StartCommand = ReactiveCommand.Create(StartLogging);
            StopCommand = ReactiveCommand.Create(StopLogging, this.WhenAnyValue(x => x.Started));
            ChooseFileCommand = ReactiveCommand.Create(SetPathToFile);
        }

        private void SetPathToFile()
        {
            var fileName = _browserService.BrowseFile();

            _fileWatcherService.StartWritingToFile(fileName);
        }

        private void StartLogging()
        {
            try
            {
                _fileWatcherService.SetPath(Path);
                _fileWatcherService.Start();

                Started = true;
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e);
            }
        }

        private void StopLogging()
        {
            _fileWatcherService.Stop();

            Started = false;
        }
    }
}
